'''
使用说明：

    本文件仅用于帮助不熟悉python语法的同学快速入门，
    主要用于读取watermelon数据集，
    可以直接使用 也可自行实现。

    
    使用时，可从外部调用相应函数，如:
        ## from watermelon import WATERMELON
        ##
        ## dataset = WATERMELON(root,path)
        ## data = dataset.data
'''

import os

import matplotlib.pyplot as plt
import numpy as np
import random
class WATERMELON(object):
    '''
    西瓜数据集
    '''
    def __init__(self,root,path):
        '''
        方法说明:
            初始化类
        参数说明:
            root: 文件夹根目录
            path: 西瓜数据集文件名 'watermelon.csv'
        '''
        self.root = root
        self.path = path
        self.data = self._get_data()

    def _get_data(self):
        #打开数据集
        with open(os.path.join(self.root,self.path),'r') as f:
            data = f.readlines()[1:]
        #去除掉西瓜编号，逗号
        for i in range(len(data)):
            data[i] = data[i].strip().split(',')[1:]
        #转化为numpy数组格式
        data = np.array(data,dtype=float)
        
        return data
    def prints(self):
        print(self.data[:])
        print(self.data.shape[0])

    def getColor(self):
        color: int

        color1 = random.randint(16, 255)
        color2 = random.randint(16, 255)
        color3 = random.randint(16, 255)
        color1 = hex(color1)
        color2 = hex(color2)
        color3 = hex(color3)
        ans = "#" + color1[2:] + color2[2:] + color3[2:]
        return ans

    def kmeans(self,k):
        #首先产生初始均值向量

        vector=[]
        dataset = {}
        dataset1={}
        aver={}
        flag=1
        for i in range(0,k):
            num=random.randint(0,self.data.shape[0]-1)
            temp=self.data[num]
            vector.append(temp)
            dataset['{}'.format(i)]=np.array(self.data[num])
            aver['{}'.format(i)]=np.array(self.data[num])
        #初始化中心选择。
        while flag!=0:
            flag=0
            for i in range(0,k):
                dataset['{}'.format(i)]=np.array([])
                dataset1['{}'.format(i)]=np.array([])
            for i in range(0,self.data.shape[0]):
                v1=self.data[i]
                mindis=10000

            #求出距离的最小值，并且用numshape记录其是哪一个簇

                for j in range(0,k):
                    vector=aver['{}'.format(j)]
                    dis=(v1[0]-vector[0])**2+(v1[1]-vector[1])**2
                    if mindis>=dis:
                        mindis=dis
                        numshape=j
                    if mindis==0:continue
                temp=dataset['{}'.format(numshape)]
                temps=np.append(temp,v1,axis=0)
                dataset['{}'.format(numshape)]=temps
                temps=dataset['{}'.format(numshape)].reshape(int(len(dataset['{}'.format(numshape)])/2),2)
                dataset1['{}'.format(numshape)]=temps
            for j in range(0,k):
                num=0
                arr=dataset1['{}'.format(j)]
                mean=arr.mean(axis=0)
                if (aver['{}'.format(j)]!=mean).any():
                    aver['{}'.format(j)]=mean
                    flag=1
                    #发生改动，继续循环

        # plt.scatter(dataset1['0'][i for i in dataset1['0']][0],dataset1['0'][i for i in dataset1['0']][1])
        for j in range(0,k):
            x = []
            y = []
            for i in dataset1['{}'.format(j)]:
                x.append(i[0])
                y.append(i[1])
            plt.scatter(x,y,color=self.getColor())
        plt.savefig('waterlemon.jpg')
        # print(dataset1['0'][i[0] for i in dataset['0']])

        return aver



s=WATERMELON('C:\\Users\\LENOVO\\OneDrive\\桌面\\aip\\kmeans','watermelon.csv')
s.kmeans(2)

